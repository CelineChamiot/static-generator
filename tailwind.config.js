const safelist = [/^(re-|is-invalid|invalid-feedback|tippy-|redactor|marker-|tooltip|alert|p-|pretty|vue-tags|ti-|graphic-gas|graphic-energy|line-clamp|energy-|gas-|f4map|kimono-map|overflow-hidden-|modal|slider|v-select$|code-toolbar|language-|map-popup$|agile|v--modal|leaflet-|mapboxgl-|vue-slider|vue-slider-|col-form-controls|datepicker|lg-|at-|dropdown-menu|multiselect|modal|swiper|page-)/]

module.exports = {
  purge: {
    mode: 'all',
    content: [
      './src/views/**/*.pug',
      './src/**/*.vue'
    ],
    preserveHtmlElements: false,
    options: {
      safelist: {
        standard: safelist,
        deep: safelist
      }
    }
  },
  theme: {
    extend: {
      screens: {
        sm: '640px',
        md: '768px',
        lg: '1024px',
        xl: '1280px'
      },
      colors: {
        'primary-100': 'var(--primary-100)',
        'primary-200': 'var(--primary-200)',
        'primary-300': 'var(--primary-300)',
        'secondary-100': 'var(--secondary-100)',
        'secondary-200': 'var(--secondary-200)',
        'secondary-300': 'var(--secondary-300)'
      },
      fontFamily: {
        'sans': ['var(--font-family-sans)'],
        'title': ['var(--font-family-title)'],
        'icon': ['var(--font-family-icon)'],
        'icon-brands': ['var(--font-family-icon-brands)']
      },
      spacing: {
        /* Components values */
        'header-height': 'var(--header-height)'
      },
      fontSize: {
        xs: '0.75rem', // 12px
        sm: '0.875rem', // 14px
        base: 'var(--text-font-size)', // 16px
        lg: '1.125rem', // 18px
        xl: '1.25rem', // 20px
        '2xl': '1.5rem', // 24px
        '3xl': '1.875rem', // 30px
        '4xl': '2.25rem', // 36px
        '5xl': '3rem', // 48px
        '6xl': '3.75rem', // 60px
        '7xl': '4.5rem', // 72px
        '8xl': '6rem', // 96px
        '9xl': '8rem' // 128px
      },
      inset: {
        /* Components values */
        'slider-arrow': '35px',
        'slider-pagination': '20px',
        'header-height': 'var(--header-height)'
      },
      minHeight: {
        'app-height': 'var(--app-height)',
        /* Components values */
        'title-thumb': '3rem'
      },
      maxWidth: {
        'container': '120rem',
        'container-xs': '81rem',
        'container-lg': '103rem'
      },
      width: {
        'container': '96%',
        '0%': '0%' // usefull for transition
      },
      height: {
        'px': '1px',
        'header-desktop': 'var(--header-height)',
        'app-height': 'var(--app-height)',
      },
      zIndex: {
        '0': '0',
        'max': '9999'
      },
      transitionProperty: {
        'animation': 'opacity, transform'
      },
      transitionDuration: {
        'default': 'var(--transition-duration-default)',
        'smooth': 'var(--transition-duration-smooth)'
      },
      transitionTimingFunction: {
        'smooth': 'var(--transition-timing-smooth)'
      }
    }
  },
  variants: {
    width: ['group-hover']
  }
}
